console.log('Задание 1');

var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0
];

var students = studentsAndPoints.filter(function(element, i) {
    return i % 2 == 0;
});

console.log(students);

var points = studentsAndPoints.filter(function(element, i) {
    return i % 2 !=0;
});

console.log(points);

console.log('Задание 2');
console.log('Список студентов:');
students.forEach(function(element, i) {
    console.log('Студент %s набрал %d баллов', element, points[i]);
});

console.log('Задание 3');

var maxBall, maxIndex;

points.forEach(function(item,i) {
    if (!maxBall || item > maxBall) {
        maxBall = item;
        maxIndex = i;
    }
});

console.log('Студент, набравший максимальный балл: %s (%d баллов)', students[maxIndex], maxBall);

console.log('Задание 4');

points = points.map(function(element, i) {
   if (students[i] == 'Ирина Овчинникова' || students[i] == 'Александр Малов') {
       return element + 30;
   } else {
       return element;
   }
});

console.log(points);

console.log('Задание 5');

function getBall(name) {
    // Позиция студента с указанным именем в массиве students
    var index = students.indexOf(name);

    return points[index];
}

function getTop(n) {
    var studentCopy = students.slice();

    studentCopy.sort(function(a, b) {
        var aBall = getBall(a);
        var bBall = getBall(b);

        return bBall - aBall;
    });

    studentCopy = studentCopy.slice(0, n);

    var result = [];

    studentCopy
        .forEach(function(student) {
            result.push(student, getBall(student));
        });

    return result;
}

function printMe(arr) {
    if (!(arr instanceof Array)) {
        return;
    }
    for (var i = 0; i < arr.length; i +=2) {
        console.log('%s — %d баллов', arr[i], arr[i + 1]);
    }
}

var top3 = getTop(3);
var top5 = getTop(5);

console.log('Топ 3:');
printMe(top3);
console.log('Топ 5:');
printMe(top5);